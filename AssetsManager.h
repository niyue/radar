//
//  AssetsManager.h
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface AssetsManager : NSObject
+ (ALAssetsLibrary *)defaultAssetsLibrary;
@end
