var target = UIATarget.localTarget();
var app = target.frontMostApp();
var mainWindow = app.mainWindow();
var tabBar = mainWindow.tabBar();

UIATarget.onAlert = function onAlert(alert) {
	alert.defaultButton().tap();
    return true;
}


function testRandomPhoto() {
    var testName = "testRandomPhoto";
    var randomTab = tabBar.elements()["Random"];
    if(randomTab.isValid()) {
        randomTab.tap();
		target.delay(1);
        UIALogger.logPass(testName);
    }
    else {
        UIALogger.logFail(testName);
    }
}

function testSettingsTab() {
	var testName = "testSettingsTab";
	tabBar.buttons()["Settings"].tap();
	mainWindow.tableViews()[0].cells()["Help"].tap();
	target.delay(1);
	app.navigationBar().buttons()["Done"].tap();
	//mainWindow.tableViews()[0].cells()["Re-index all photos"].tap();
	UIALogger.logPass(testName);
}

function testSearchPhotos() {
	var testName = "testSearchPhotos";
    var searchTab = tabBar.elements()["Search"];
	searchTab.tap();
	app.navigationBar().buttons()["Search"].tap();
	target.delay(6);
	var photosTableView = mainWindow.tableViews()[0];
	photosTableView.cells()[0].tap();
	mainWindow.buttons()[0].tap();
	
	mainWindow.tableViews()[0].cells()["EXIF"].tap();
	target.frontMostApp().navigationBar().leftButton().tap();
	mainWindow.tableViews()[0].cells()["TIFF"].tap();
	target.frontMostApp().navigationBar().leftButton().tap();
	mainWindow.tableViews()[0].cells()["GPS"].tap();
	target.frontMostApp().navigationBar().leftButton().tap();
	
	mainWindow.tableViews()[0].cells()["Location"].tap();
	target.delay(2);
	//mainWindow.scrollViews()[0].elements()[0].tap();
	target.frontMostApp().navigationBar().leftButton().tap();
	

	mainWindow.navigationBar().rightButton().tap();
	UIALogger.logPass(testName);
}

testRandomPhoto();
testSettingsTab();
testSearchPhotos();