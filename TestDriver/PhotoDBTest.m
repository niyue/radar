//
//  PhotoDBTest.m
//  PhotoDB
//
#import <GHUnitIOS/GHUnit.h>
#import "PhotoDB.h"
#import "AssetsReader.h"
#import "TestAssetProcessor.h"

@interface PhotoDBTest : GHTestCase 
{
    
}
- (NSArray *)allPhotos;
@end

@implementation PhotoDBTest
- (NSArray *)allPhotos {
    AssetsReader *reader = [[AssetsReader alloc] init];
    TestAssetProcessor *assetProcessor = [[TestAssetProcessor alloc] init];
    reader.assetProcessor = assetProcessor;
    [reader photos];
    NSArray *photos = assetProcessor.photos;
    return photos;
}

- (void)testOpenDB {
    PhotoDB *db = [[PhotoDB alloc] init];
    BOOL opened = [db open];
    GHAssertTrue(opened, @"Photo db should be opened.");
    [db close];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dbPath = [db dbLocation];
    BOOL dbExisted = [fileManager fileExistsAtPath:dbPath];
    GHAssertTrue(dbExisted, @"Photo db should exist.");
}

- (void)testCleanDB {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db open];
    [db close];
    [db clean];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dbPath = [db dbLocation];
    BOOL dbExisted = [fileManager fileExistsAtPath:dbPath];
    GHAssertFalse(dbExisted, @"Photo db should not exist.");
}

- (void)testInitialCount {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db clean];
    [db open];
    int count = [db count];
    GHAssertEquals(count, 0, @"Initial photo count should be zero");
    [db close];
}

- (void)testIndexDb {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db clean];
    [db open];
    NSArray *photos = [self allPhotos];    
    int count = [photos count];
    GHAssertGreaterThan(count, 0, @"There should be at least one photo fetched by reader");
    
    [db indexPhotos:photos];
    count = [db count];
    GHAssertGreaterThan(count, 0, @"There should be at least one photo");
    [db close];
}

- (void)testQueryByDate {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db clean];
    [db open];
    NSArray *photos = [self allPhotos];
    [db indexPhotos:photos];
    int count = [db count];
    GHAssertGreaterThan(count, 0, @"There should be at least one photo");
    NSArray *matchedPhotos = [db queryByDate:@"2011-02-15 00:00:00" toDate:@"2011-02-15 23:59:59"];
    int resultSize = [matchedPhotos count];
    GHAssertEquals(resultSize, 4, @"There should be 4 photos in the date range");
    GHAssertGreaterThan(count, resultSize, @"The matched photos is a subset of all photos");
    [db close];
}

- (void)testQueryByCamera {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db clean];
    [db open];
    NSArray *photos = [self allPhotos];
    [db indexPhotos:photos];
    int count = [db count];
    GHAssertGreaterThan(count, 0, @"There should be at least one photo");
    NSArray *matchedPhotos = [db queryByCamera:10000 maxShutterSpeed:16000 minISOSpeed:64 maxISOSpeed:640];
    int resultSize = [matchedPhotos count];
    NSLog(@"There are %d photos matched", resultSize);
    GHAssertEquals(resultSize, 1, @"There should be 1 photos in the date range and shutter speed");
    GHAssertGreaterThan(count, resultSize, @"The matched photos is a subset of all photos");
    [db close];
}

- (void)testQueryMultipleConditions {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db clean];
    [db open];
    NSArray *photos = [self allPhotos];
    [db indexPhotos:photos];
    int count = [db count];
    GHAssertGreaterThan(count, 0, @"There should be at least one photo");
    NSArray *matchedPhotos = [db query:@"2011-02-15 00:00:00" toDate:@"2011-02-15 23:59:59" minShutterSpeed:1000 maxShutterSpeed:2000 minISOSpeed:64 maxISOSpeed:640];
    int resultSize = [matchedPhotos count];
    NSLog(@"There are %d photos matched", resultSize);
    GHAssertEquals(resultSize, 2, @"There should be 2 photos in the date range and shutter speed");
    GHAssertGreaterThan(count, resultSize, @"The matched photos is a subset of all photos");
    [db close];
}

- (void)testPhotoAtIndex {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db clean];
    [db open];
    NSArray *photos = [self allPhotos];
    [db indexPhotos:photos];
    NSDictionary *photoInfo = [db photoAtIndex:1];
    NSString *photoUrlString = [photoInfo valueForKey:@"url"];
    NSURL *photoUrl = [NSURL URLWithString:photoUrlString];
    GHAssertNotNil(photoUrl, @"Photo URL should not be nil");
    [db close];
}

@end
