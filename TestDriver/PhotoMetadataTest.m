//
//  PhotoMetadataTest.m
//  PhotoDB
//

#import <GHUnitIOS/GHUnit.h>
#import "AssetsReader.h"
#import "ALAsset+AssetURL.h"
#import "AssetProcessor.h"
#import "TestAssetProcessor.h"
#import "PhotoMetadata.h"

@interface PhotoMetadataTest : GHTestCase 
{
    
}
@end

@implementation PhotoMetadataTest

- (void)testReadPhotoMetadata
{
    AssetsReader *reader = [[AssetsReader alloc] init];
    TestAssetProcessor *assetProcessor = [[TestAssetProcessor alloc] init];
    reader.assetProcessor = assetProcessor;
    [reader photos];
    NSArray *photos = assetProcessor.photos;
    GHAssertTrue([photos count] > 0, @"There should be at least one photo in the library");
    NSEnumerator *enumerator = [photos objectEnumerator];
    ALAsset *photo;
    while ((photo = [enumerator nextObject])) {
        NSDictionary *metadata = [[photo defaultRepresentation] metadata];
        PhotoMetadata *photoMetadata = [[PhotoMetadata alloc] initWithDict:metadata];
        GHAssertNotNil([photoMetadata dateTimeOriginal], @"Photo creation date should not be nil");
        NSLog(@"The photo creation date is: %@", [photoMetadata dateTimeOriginal]);
        GHAssertNotNil([photoMetadata timeOriginal], @"Photo creation time should not be nil");
        NSInteger shutterSpeed = [photoMetadata shutterSpeed];
        NSLog(@"The shutter speed is: %d", shutterSpeed);
        GHAssertTrue(shutterSpeed >= 1 && shutterSpeed <= 16000, @"Shutter speed should be less than 1s and larger than 1/4000s");
        NSInteger isoSpeed = [photoMetadata isoSpeed];
        NSLog(@"The ISO speed is: %d", isoSpeed);
        GHAssertTrue(isoSpeed >= 10 && isoSpeed <= 6400, @"ISO speed should be between 10 and 6400");
        GHAssertTrue([photoMetadata focalLength] > 0, @"Focal length should be greater than 0");
        GHAssertTrue([photoMetadata fnumber] > 0, @"FNumber should be greater than 0");
    }
}

@end
