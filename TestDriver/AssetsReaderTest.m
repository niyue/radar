//
//  AssetsReaderTest.m
//
#import <GHUnitIOS/GHUnit.h>
#import "AssetsReader.h"
#import "ALAsset+AssetURL.h"
#import "AssetProcessor.h"
#import "TestAssetProcessor.h"

@interface AssetsReaderTest : GHTestCase 
{
    
}
@end

@implementation AssetsReaderTest

- (void)testLoadGroup {
    AssetsReader *reader = [[AssetsReader alloc] init];
    NSArray *groups = [reader groups];
    int count = [groups count];
    NSLog(@"Group size is %i", count);
    GHAssertGreaterThan(count, 0, @"There should be at least one group");
}

- (void)testLoadPhotosWithAssetProcessor {
    AssetsReader *reader = [[AssetsReader alloc] init];
    TestAssetProcessor *assetProcessor = [[TestAssetProcessor alloc] init];
    reader.assetProcessor = assetProcessor;
    [reader photos];
    NSArray *photos = assetProcessor.photos;
    int count = [photos count];
    NSLog(@"Photos count is %i", count);
    GHAssertGreaterThan(count, 0, @"There should be at least one photo");
}

- (void)testLoadAssetViaUrl {
    AssetsReader *reader = [[AssetsReader alloc] init];
    TestAssetProcessor *assetProcessor = [[TestAssetProcessor alloc] init];
    reader.assetProcessor = assetProcessor;
    [reader photos];
    NSArray *photos = assetProcessor.photos;
    
    NSEnumerator *enumerator = [photos objectEnumerator];
    ALAsset *photo;
    while ((photo = [enumerator nextObject])) {
        NSURL *photoUrl = [photo url];
        AssetsReader *assetReader = [[AssetsReader alloc] init];
        ALAsset *asset = [assetReader asset:photoUrl];
        GHAssertNotNil(asset, @"Asset should be loaded for key %@", photoUrl);
    }
}

- (void)testLoadAssetViaInvalidUrl {
    // there is no such photo URL
    NSURL *photoUrl = [NSURL URLWithString:@"assets-library://asset/asset.JPG?id=1B134D5C-AC3E-4B2B-AB45-C82BAAAAAAAA&ext=JPG"];
    AssetsReader *reader = [[AssetsReader alloc] init];
    ALAsset *photo = [reader asset:photoUrl];
    GHAssertNil(photo, @"Photo should be nil for invalid URL.");
}

@end
