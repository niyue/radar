//
//  DateHelperTest.m
//  PhotoDB
//
#import <GHUnitIOS/GHUnit.h>
#import "DateHelper.h"

@interface DateHelperTest : GHTestCase 
{
    
}
@end

@implementation DateHelperTest
- (void)testToday {
    NSString *currentDate = [DateHelper endOfToday];
    GHAssertNotNil(currentDate, @"Current date should not be nil");
}

- (void)testNow {
    NSString *now = [DateHelper now];
    GHAssertNotNil(now, @"Now should not be nil");
}

- (void)testStartOfSixMonthsAgo {
    NSString *startOfSixMonthsAgo = [DateHelper startOfSixMonthsAgo];
    GHAssertNotNil(startOfSixMonthsAgo, @"Six months ago should not be nil");
}

- (void)testDateFromStringDateAndMinute {
    NSDate *date = [DateHelper valueOf:@"2011-02-02 02:10"];
    GHAssertNotNil(date, @"Now should not be nil");
}
@end
