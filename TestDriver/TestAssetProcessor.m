//
//  TestAssetProcessor.m
//  PhotoDB
//

#import "TestAssetProcessor.h"
#import "ALAsset+AssetURL.h"

@implementation TestAssetProcessor
@synthesize photos;

- (TestAssetProcessor *) init {
    self = [super init];
    if (self) {
        self.photos = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)processPhoto:(ALAsset *)photo {
    [photos addObject:photo];
    NSLog(@"Photo %@ is processed", [photo url]);
}

- (void)processGroup:(ALAssetsGroup *)group {
    NSLog(@"%ld photos in the group", (long)[group numberOfAssets]);
}

- (void)complete {
    NSLog(@"Photo processing completed");
}
@end
