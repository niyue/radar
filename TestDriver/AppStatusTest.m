//
//  AppStatusTest.m
//

#import <GHUnitIOS/GHUnit.h>
#import "AppStatus.h"

@interface AppStatusTest : GHTestCase 
{
    
}
@end

@implementation AppStatusTest

- (void)testUpdateIndex {
    GHAssertNil([AppStatus lastIndexTime], @"Initial last index time should be nil.");
    GHAssertEquals([AppStatus lastIndexPhotoNumbers], 0, @"Initial last index photo number should be nil.");
    NSDate *now = [[NSDate alloc] init];
    [AppStatus updateIndex:[now description] photoNumber:1000];
    GHAssertNotNil([AppStatus lastIndexTime], @"Last index time should not be nil");
    GHAssertEquals(1000, [AppStatus lastIndexPhotoNumbers], @"Last index photo number should not be nil");
    [AppStatus updateIndex:nil photoNumber:0];
}

- (void)testIsFirstRun {
    GHAssertTrue([AppStatus isFirstRun], @"It is first run initially.");
    [AppStatus updateFirstRun:NO];
    GHAssertFalse([AppStatus isFirstRun], @"It is not first run now.");
    [AppStatus updateFirstRun:YES];
}

@end
