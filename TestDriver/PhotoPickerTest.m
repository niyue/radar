//
//  AssetsReaderTest.m
//  PhotoDB
//
#import <GHUnitIOS/GHUnit.h>
#import "PhotoPicker.h"
#import "PhotoDB.h"
#import "AssetsReader.h"
#import "TestAssetProcessor.h"

@interface PhotoPickerTest : GHTestCase 
{
    
}
- (void)indexAllPhotos;
- (NSArray *)allPhotos;
@end

@implementation PhotoPickerTest
- (void)indexAllPhotos {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db open];
    [db indexPhotos:[self allPhotos]];
    [db close];
}
     
- (NSArray *)allPhotos {
    AssetsReader *reader = [[AssetsReader alloc] init];
    TestAssetProcessor *assetProcessor = [[TestAssetProcessor alloc] init];
    reader.assetProcessor = assetProcessor;
    [reader photos];
    NSArray *photos = assetProcessor.photos;
    return photos;
}

- (void)testRandomPhoto {
    [self indexAllPhotos];
    PhotoPicker *photoPicker = [[PhotoPicker alloc] init];
    ALAsset *photo = [photoPicker random];
    GHAssertNotNil(photo, @"There should be a random photo");
}
@end
