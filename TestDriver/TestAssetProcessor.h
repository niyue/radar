//
//  TestAssetProcessor.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>
#import "AssetProcessor.h"

@interface TestAssetProcessor : NSObject <AssetProcessor>
{
    NSMutableArray *photos;
}
@property (nonatomic, retain) NSArray *photos;
@end
