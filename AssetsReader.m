//
//  AssetsReader.m
//

#import "AssetsReader.h"


@implementation AssetsReader
@synthesize assetProcessor;

- (NSArray *)groups
{
    NSCondition *done = [[NSCondition alloc] init];
    __block BOOL allGroupsEnumerated = false;
    NSMutableArray *groups = [[NSMutableArray alloc] initWithCapacity:5];
    
    NSBlockOperation* loadGroupsOp = [NSBlockOperation blockOperationWithBlock: ^{
        void (^groupEnumerator)(ALAssetsGroup*, BOOL*) = ^(ALAssetsGroup *group, BOOL *stop) {
            if(group != nil) {
                NSLog(@"[Groups] Add group '%@'", [group valueForProperty:ALAssetsGroupPropertyName]);
                [groups addObject:group];
            }
            else {
                NSLog(@"[Groups] No more groups");
                allGroupsEnumerated = true;
                [done signal];
            }
        };
        
        ALAssetsLibrary* library = [AssetsManager defaultAssetsLibrary];
        [library enumerateGroupsWithTypes: ALAssetsGroupAll
                               usingBlock:groupEnumerator
                             failureBlock:^(NSError * err) {NSLog(@"Erorr: %@", [err localizedDescription]);}];
    }];
    
    NSOperationQueue *opQueue = [[NSOperationQueue alloc] init];
    [opQueue addOperation:loadGroupsOp];

    [done lock];
    while (!allGroupsEnumerated) {
        [done wait];
    }
    [done unlock];
    
    return groups;
}

- (void)photos
{
    NSCondition *done = [[NSCondition alloc] init];
    __block BOOL allGroupsEnumerated = false;
    
    NSBlockOperation* loadPhotosOp = [NSBlockOperation blockOperationWithBlock: ^{
        void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *asset, NSUInteger index, BOOL *stop) {
            if(asset != nil) {
                NSLog(@"[Assets] Add asset %@", asset);
                if (assetProcessor != nil) {
                    [assetProcessor processPhoto:asset];
                }
            }
        };
        
        void (^groupEnumerator)(ALAssetsGroup*, BOOL*) = ^(ALAssetsGroup *group, BOOL *stop) {
            if(group != nil) {
                NSLog(@"[Groups] Enumerate assets in group '%@'", [group valueForProperty:ALAssetsGroupPropertyName]);
                if (assetProcessor != nil) {
                    [assetProcessor processGroup:group];
                }
                [group setAssetsFilter:ALAssetsFilter.allPhotos];
                [group enumerateAssetsUsingBlock:assetEnumerator];
            }
            else {
                NSLog(@"[Groups] No more groups");
                if (assetProcessor != nil) {
                    [assetProcessor complete];
                }
                allGroupsEnumerated = true;
                [done signal];
            }
        };
        
        ALAssetsLibrary* library = [AssetsManager defaultAssetsLibrary];
        [library enumerateGroupsWithTypes: ALAssetsGroupSavedPhotos
                               usingBlock:groupEnumerator
                             failureBlock:^(NSError * err) {NSLog(@"Erorr: %@", [err localizedDescription]);}];
    }];
    
    NSOperationQueue *opQueue = [[NSOperationQueue alloc] init];
    [opQueue addOperation:loadPhotosOp];
    
    [done lock];
    while (!allGroupsEnumerated) {
        [done wait];
    }
    [done unlock];
}

- (ALAsset *)asset:(NSURL *)assetUrl {
    __block BOOL assetLoaded = false;
    NSCondition *done = [[NSCondition alloc] init];
    __block ALAsset *loadedAsset = nil;
    
    NSBlockOperation* loadAssetOp = [NSBlockOperation blockOperationWithBlock: ^{
        ALAssetsLibrary* library = [AssetsManager defaultAssetsLibrary];    
        void (^assetBlock)(ALAsset *asset) = ^(ALAsset *asset) {
            loadedAsset = asset;
            assetLoaded = true;
            [done signal];
        };
        [library assetForURL:assetUrl 
                 resultBlock:assetBlock 
                failureBlock:^(NSError *err) {NSLog(@"Erorr: %@", [err localizedDescription]);}];
    }];
    
    NSOperationQueue *opQueue = [[NSOperationQueue alloc] init];
    [opQueue addOperation:loadAssetOp];
    
    [done lock];
    while (!assetLoaded) {
        [done wait];
    }
    [done unlock];
    return loadedAsset;
}
@end
