//
//  AssetsReader.h
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AssetsManager.h"
#import "AssetProcessor.h"

@protocol AssetProcessor;

@interface AssetsReader : NSObject
{
    id<AssetProcessor> assetProcessor;
}
- (NSArray *) groups;
- (void) photos;
- (ALAsset *) asset:(NSURL *)assetUrl;
@property (nonatomic, retain) id<AssetProcessor> assetProcessor;
@end
