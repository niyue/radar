//
//  PhotoViewController.m
//  PhotoDB
//

#import "PhotoViewController.h"
#import "PhotoInfoNavigationController.h"
#import "ALAsset+AssetURL.h"

@implementation PhotoViewController
@synthesize photo;
@synthesize photoView;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    CGImageRef fullScreenImage = [[photo defaultRepresentation] fullScreenImage];
    photoView.image = [[UIImage alloc] initWithCGImage:fullScreenImage];
    photoView.delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TapDetectingImageViewDelegate
- (void)tapDetectingImageView:(TapDetectingImageView *)view gotSingleTapAtPoint:(CGPoint)tapPoint
{
    [self toggleNavigationBar];
}

- (void)toggleNavigationBar
{
    BOOL hidden = [self.navigationController isNavigationBarHidden];
    [self.navigationController setNavigationBarHidden:!hidden animated:YES];
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowPhotoInfoSegue"])
    {
        PhotoInfoNavigationController *photoInfoNavController = [segue destinationViewController];
        photoInfoNavController.photo = photo;
    }
}

#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return photoView;
}

@end
