//
//  PhotoLocationAnnotation.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PhotoLocationAnnotation : NSObject <MKAnnotation>
{
    CLLocation *location;
    CLLocationCoordinate2D coordinate;
    NSString *locationTitle;
}
- (id)initWithLocation:(CLLocation *)location;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSString *locationTitle;
@end
