//
//  SearchCategoryViewController.m
//  PhotoDB
//

#import "SearchCategoryViewController.h"
#import "SearchCategory.h"
#import "SearchViewController.h"
#import "DateSearchCategory.h"
#import "CameraSearchCategory.h"
#import "CompoundSearchCategory.h"
#import "AppStatus.h"
#import "TutorialViewController.h"

@interface SearchCategoryViewController ()
@end

@implementation SearchCategoryViewController
@synthesize searchCategoriesTable;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([AppStatus isFirstRun]) {
        UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        TutorialViewController *tutorialController = [storybord instantiateViewControllerWithIdentifier:@"TutorialViewController"];
        [self.tabBarController presentViewController:tutorialController animated:YES completion:nil];
    }
    
    searchCategoryIdentifiers = [[NSArray alloc] initWithObjects:@"DateSearchCategoryCell", @"CameraSearchCategoryCell", @"CompoundSearchCategoryCell", nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchCategoryIdentifiers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger currentRow = [indexPath row];
    NSString *cellIdentifier = [searchCategoryIdentifiers objectAtIndex:currentRow];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id<SearchCategory> searchCategory = nil;
    if ([[segue identifier] isEqualToString:@"DateSearchSegue"])
    {
        searchCategory = [[DateSearchCategory alloc] init];
    }
    else if ([[segue identifier] isEqualToString:@"CameraSearchSegue"])
    {
        searchCategory = [[CameraSearchCategory alloc] init];
    }
    else if ([[segue identifier] isEqualToString:@"CompoundSearchSegue"])
    {
        searchCategory = [[CompoundSearchCategory alloc] init];
    }
    if (searchCategory != nil) {
        SearchViewController *searchViewController = [segue destinationViewController];
        searchViewController.searchCategory = searchCategory;
    }
}

@end
