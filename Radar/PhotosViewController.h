//
//  PhotosViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>


@interface PhotosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSArray *photos;
    UITableView *photosTableView;
    ALAsset *selectedPhoto;
}
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, retain) IBOutlet UITableView *photosTableView;
@end
