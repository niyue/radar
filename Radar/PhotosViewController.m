//
//  PhotosViewController.m
//  PhotoDB
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/CGImageProperties.h>
#import "PhotosViewController.h"
#import "PhotoTableViewCell.h"
#import "ALAsset+AssetURL.h"
#import "AssetsReader.h"
#import "PhotoViewController.h"
#import "PhotoMetadata.h"


@implementation PhotosViewController
@synthesize photos;
@synthesize photosTableView;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = [NSString stringWithFormat:@"%d photos", [photos count]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [photos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PhotoCell";
    
    PhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[PhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *photoInfo = [photos objectAtIndex:[indexPath row]];
    NSString *photoUrlString = [photoInfo valueForKey:@"url"];
    NSURL *photoUrl = [NSURL URLWithString:photoUrlString];
    AssetsReader *reader = [[AssetsReader alloc] init];
    ALAsset *photo = [reader asset:photoUrl];
    
    ALAssetRepresentation *representation = [photo defaultRepresentation];

    PhotoMetadata *metadata = [[PhotoMetadata alloc] initWithDict:[representation metadata]];

    NSString *exifDateTimeOriginal = [metadata dateTimeOriginal];
    cell.creationDateLabel.text = exifDateTimeOriginal;
    NSInteger shutterSpeed = [metadata shutterSpeed];
    cell.shutterSpeedLabel.text = [NSString stringWithFormat:@"Shutter: 1/%ds", shutterSpeed];

    NSInteger isoSpeed = [metadata isoSpeed];
    cell.isoSpeedLabel.text = [NSString stringWithFormat:@"ISO: %d", isoSpeed];
    
    UIImage *thumbnail = [[UIImage alloc] initWithCGImage:[photo thumbnail]];
    cell.thumbnail.image = thumbnail;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DisplayPhotoSegue"])
    {
        NSIndexPath *indexPath = [photosTableView indexPathForSelectedRow];
        
        NSDictionary *photoInfo = [photos objectAtIndex:[indexPath row]];
        NSString *photoUrlString = [photoInfo valueForKey:@"url"];
        NSURL *photoUrl = [NSURL URLWithString:photoUrlString];
        AssetsReader *reader = [[AssetsReader alloc] init];
        ALAsset *photo = [reader asset:photoUrl];
        
        PhotoViewController *photoViewController = [segue destinationViewController];
        photoViewController.photo = photo;
        UINavigationController *navController = self.navigationController;
        [navController setNavigationBarHidden:YES animated:YES];
        navController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    }
}

@end
