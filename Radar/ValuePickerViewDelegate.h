//
//  ValuePickerViewDelegate.h
//  PhotoDB
//

#import <Foundation/Foundation.h>

@protocol ValuePickerViewDelegate <NSObject>
-(void)didPickValue:(NSString *)pickedValue;
@end
