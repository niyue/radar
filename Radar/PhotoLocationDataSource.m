//
//  PhotoLocationDataSource.m
//  PhotoDB
//

#import "PhotoLocationDataSource.h"
#import <AddressBookUI/AddressBookUI.h>

@implementation PhotoLocationDataSource
@synthesize location;

- (PhotoLocationDataSource *)initWithLocation:(CLLocation *)photoLocation
{
    self = [super init];
    if (self) {
        self.location = photoLocation;
        self->requireRefresh = YES;
    }
    return self;
}

- (NSString *)cellIdentifier
{
    return @"PhotoLocationCell";
}

- (NSString *)nameAtIndex:(int)row
{
    return @"Location";
}

- (NSString *)textAtIndex:(int)row
{
    return @"Location";
}

- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *text))completionBlock
{
    if (placeName == nil) {
        CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if ([placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                self->placeName = [placemark name];
                completionBlock(placeName);
            }
        }];
    }
    else {
        requireRefresh = NO;
        completionBlock(placeName);
    }
    
    return @"";
}

- (int)count
{
    return 1;
}

- (BOOL)requireRefresh
{
    return requireRefresh;
}
@end
