//
//  PhotoInfoDataSource.h
//  PhotoDB
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@protocol PhotoInfoDataSource <NSObject>
- (NSString *)cellIdentifier;
- (NSString *)nameAtIndex:(int)row;
- (NSString *)textAtIndex:(int)row;
- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *text))completionBlock;
- (int)count;
- (BOOL)requireRefresh;
@end
