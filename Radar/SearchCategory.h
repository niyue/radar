//
//  SearchCondition.h
//  PhotoDB
//

#import <Foundation/Foundation.h>

@protocol SearchCategory <NSObject>

- (NSArray *)search:(NSArray *)params;

@property (nonatomic, retain) NSArray *identifiers;
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *labels;
@property (nonatomic, retain) NSArray *defaultValues;
@property (nonatomic, retain) NSArray *values;
@end
