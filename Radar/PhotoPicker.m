//
//  PhotoPicker.m
//  PhotoDB
//

#include <stdlib.h>
#import "PhotoPicker.h"
#import "PhotoDB.h"
#import "AssetsReader.h"

@interface PhotoPicker ()
-(int)randomNumber:(int)max;
-(ALAsset *)getRandomPhoto:(PhotoDB *)db;
@end

@implementation PhotoPicker
- (ALAsset *)random {
    PhotoDB *db = [[PhotoDB alloc] init];
    [db open];
    ALAsset *randomPhoto = [self getRandomPhoto:db];
    [db close];
    return randomPhoto;
}

- (ALAsset *)getRandomPhoto:(PhotoDB *)db {
    int count = [db count];
    ALAsset *randomPhoto = nil;
    if (count > 0) {
        int randomNumber = [self randomNumber:count];
        NSDictionary *photoInfo = [db photoAtIndex:randomNumber];
        NSString *photoUrlString = [photoInfo valueForKey:@"url"];
        NSURL *photoUrl = [NSURL URLWithString:photoUrlString];
        AssetsReader *reader = [[AssetsReader alloc] init];
        ALAsset *photo = [reader asset:photoUrl];
        randomPhoto = photo;
    }
    return randomPhoto;
}

- (int)randomNumber:(int)max {
    return arc4random() % max;
}
@end
