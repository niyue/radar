//
//  CameraSearchCategory.h
//  PhotoDB
//

#import <Foundation/Foundation.h>
#import "SearchCategory.h"

@interface CameraSearchCategory : NSObject<SearchCategory>
{
    NSArray *identifiers;
    NSArray *headers;
    NSArray *labels;
    NSArray *defaultValues;
    NSArray *values;
}

@end
