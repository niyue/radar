//
//  SettingsViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView *settingsTableView;
}
@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;
@end
