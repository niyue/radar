//
//  LayerAnimation.m
//  PhotoDB
//

#import "LayerAnimation.h"
#import <QuartzCore/QuartzCore.h>

@implementation LayerAnimation

+ (void)fadeIn:(UIView *)view {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [view.layer addAnimation:transition forKey:nil];
}

@end
