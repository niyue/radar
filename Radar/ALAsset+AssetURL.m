//
//  ALAsset+AssetURL.m
//

#import "ALAsset+AssetURL.h"

@implementation ALAsset (AssetURL)
- (NSURL *)url
{
    NSDictionary *urls = [self valueForProperty:ALAssetPropertyURLs];
    NSEnumerator *enumerator = [urls keyEnumerator];
    id key = [enumerator nextObject];
    NSURL *url = [urls valueForKey:key];
    return url;
}
@end
