//
//  UITableView+CellDetailText.h
//  PhotoDB
//
//

#import <UIKit/UIKit.h>

@interface UITableView (CellDetailText)
- (NSString *)detailTextForCell:(NSUInteger)cellRow;
- (NSString *)detailTextForCell:(NSUInteger)cellSection cellRow:(NSUInteger)cellRow;
@end
