//
//  RandomPhotoController.m
//  PhotoDB
//

#import "RandomPhotoController.h"
#import "PhotoDB.h"
#import "PhotoPicker.h"
#import "PhotoInfoNavigationController.h"
#import "LayerAnimation.h"
#import <QuartzCore/QuartzCore.h>


@interface RandomPhotoController()
- (void)loadPhoto;
- (void)pickRandomPhoto;
- (void)stopHUD;
@end

@implementation RandomPhotoController
@synthesize imageView;
@synthesize loadingIndicator;
@synthesize hudView;
@synthesize swipeLeftGestureRecognizer;
@synthesize swipeRightGestureRecognizer;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    hudView.layer.cornerRadius = 10.0;
    [self loadPhoto];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)nextRandomPhoto:(id)sender
{
    [self loadPhoto];
}

// private methods
- (void)stopHUD
{
    [UIView transitionWithView:hudView 
            duration:0.5
            options:UIViewAnimationOptionCurveEaseIn
            animations:^{hudView.alpha = 0.0;} 
            completion:^(BOOL finished){[hudView removeFromSuperview];}];
}

- (void)pickRandomPhoto {
    PhotoPicker *photoPicker = [[PhotoPicker alloc] init];
    self->photo = [photoPicker random];
    CGImageRef fullScreenImage = [[self->photo defaultRepresentation] fullScreenImage];
    [LayerAnimation fadeIn:self.imageView];
    imageView.image = [[UIImage alloc] initWithCGImage:fullScreenImage];
    [loadingIndicator stopAnimating];
    [self performSelector:@selector(stopHUD) withObject:nil afterDelay:1]; 
}
     

- (void)loadPhoto
{
    [loadingIndicator startAnimating];
    NSBlockOperation* loadPhotoOp = [NSBlockOperation blockOperationWithBlock: ^{
        [self performSelectorOnMainThread:@selector(pickRandomPhoto) withObject:nil waitUntilDone:NO];
    }];
    
    NSOperationQueue *opQueue = [[NSOperationQueue alloc] init];
    [opQueue addOperation:loadPhotoOp];
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.type == UIEventSubtypeMotionShake) {
        [self loadPhoto];
    }
}

#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DisplayPhotoInfoSegue"])
    {
        PhotoInfoNavigationController *photoInfoNavController = [segue destinationViewController];
        photoInfoNavController.photo = photo;
    }
}
@end
