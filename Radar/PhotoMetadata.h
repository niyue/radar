//
//  PhotoMetadata.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>

@interface PhotoMetadata : NSObject
{
    NSDictionary *metadata;
    NSDictionary *exif;
}
-(PhotoMetadata *)initWithDict:(NSDictionary *)dict;
-(NSString *)dateTimeOriginal;
-(NSString *)timeOriginal;
-(NSInteger)shutterSpeed;
-(NSInteger)isoSpeed;
-(double)focalLength;
-(double)fnumber;
@end
