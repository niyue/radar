//
//  PhotoMapperViewController.m
//  PhotoDB
//

#import "PhotoMapperViewController.h"
#import "PhotoLocationAnnotation.h"

@implementation PhotoMapperViewController
@synthesize mapView;
@synthesize location;
@synthesize callOutImage;
@synthesize captureDate;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    PhotoLocationAnnotation *annotation = [[PhotoLocationAnnotation alloc] initWithLocation:location];
    annotation.locationTitle = captureDate;
    [mapView addAnnotation:annotation]; 
    
    MKCoordinateRegion region = {location.coordinate, {0.04f, 0.04f}}; 
    [mapView setRegion:region animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - map view delegate
- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id ) annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MKPinAnnotationView *photoAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PhotoLocation"];
    photoAnnotationView.pinColor = MKPinAnnotationColorRed;
    photoAnnotationView.animatesDrop = YES; 
    photoAnnotationView.canShowCallout = YES;
    UIImageView *callOutImageView = [[UIImageView alloc] initWithImage: callOutImage];
    callOutImageView.frame = CGRectMake(0, 0, 32, 32);
    photoAnnotationView.leftCalloutAccessoryView = callOutImageView;
    
    [photoAnnotationView setSelected:YES animated:YES]; 
    return photoAnnotationView;
}

@end
