//
//  PhotoPropertiesDataSource.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>
#import "PhotoInfoDataSource.h"

@interface PhotoPropertiesDataSource : NSObject <PhotoInfoDataSource>
{
    NSArray *properties;
    NSArray *values;
}
- (PhotoPropertiesDataSource *)initWithMetadata:(NSDictionary *)metadata;
- (NSString *)cellIdentifier;
- (NSString *)nameAtIndex:(int)row;
- (NSString *)textAtIndex:(int)row;
- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *))completionBlock;
- (int)count;
@end
