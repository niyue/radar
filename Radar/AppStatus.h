//
//  AppStatus.h
//

#import <Foundation/Foundation.h>

@interface AppStatus : NSObject
+(void)updateIndex:(NSString *)indexTime photoNumber:(NSInteger)number;
+(NSString *)lastIndexTime;
+(NSInteger)lastIndexPhotoNumbers;
+(BOOL)isFirstRun;
+(void)updateFirstRun:(BOOL)isFirstRun;
@end


