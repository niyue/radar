//
//  LayerAnimation.h
//

#import <Foundation/Foundation.h>

@interface LayerAnimation : NSObject
+(void)fadeIn:(UIView *)view;
@end
