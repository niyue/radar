//
//  DateHelper.m
//  PhotoDB
//

#import "DateHelper.h"

@implementation DateHelper
+ (NSString *)endOfToday {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [[NSDate alloc] init];
    NSString *endOfToday = [NSString stringWithFormat:@"%@ 23:59", [df stringFromDate:today]];
    return endOfToday;
}

+ (NSString *)now {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *today = [[NSDate alloc] init];
    return [df stringFromDate:today];
}

+ (NSString *)startOfSixMonthsAgo {
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth:-6];
    NSDate *oneMonthsAgo = [gregorian dateByAddingComponents:offsetComponents toDate:today options:0];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *oneMonthAgoDate = [df stringFromDate:oneMonthsAgo];
    return [NSString stringWithFormat:@"%@ 00:00", oneMonthAgoDate];
}

+ (NSDate *)valueOf:(NSString *)dateAndMinute {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [df dateFromString:dateAndMinute];
    return date;
}
@end
