//
//  CompoundSearchCategory.h
//  PhotoDB
//

#import <Foundation/Foundation.h>
#import "SearchCategory.h"

@interface CompoundSearchCategory : NSObject <SearchCategory>
{
    NSMutableArray *identifiers;
    NSMutableArray *headers;
    NSMutableArray *labels;
    NSMutableArray *defaultValues;
    NSMutableArray *values;
}

@end
