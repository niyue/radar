//
//  PhotoInfoViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "PhotoPropertiesDataSource.h"
#import "MetadataGroupsDataSource.h"
#import "PhotoLocationDataSource.h"

@interface PhotoInfoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    ALAsset *photo;
    UITableView *metadataTableView;
    NSMutableDictionary *tableSections;
    NSDictionary *metadata;
    NSArray *dataSources;
    CLLocation *location;
}
@property (nonatomic, retain) ALAsset *photo;
@property (nonatomic, retain) IBOutlet UITableView *metadataTableView;
@property (nonatomic, retain) CLLocation *location;
- (IBAction)dismiss:(id)sender;
@end
