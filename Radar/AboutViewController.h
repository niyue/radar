//
//  AboutViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface AboutViewController : UIViewController <MFMailComposeViewControllerDelegate>
{

}

- (IBAction)showMailComposer:(id)sender;

@end
