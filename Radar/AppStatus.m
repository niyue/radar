//
//  AppStatus.m
//

#import "AppStatus.h"

@interface AppStatus ()

@end

@implementation AppStatus

+(void)updateIndex:(NSString *)indexTime photoNumber:(NSInteger)number
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:indexTime forKey:@"lastIndexTime"];
    [userDefaults setInteger:number forKey:@"lastIndexPhotoNumbers"];
    [userDefaults synchronize];
}

+(NSString *)lastIndexTime
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastIndexTime"];
}

+(NSInteger)lastIndexPhotoNumbers
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"lastIndexPhotoNumbers"];
}

+(BOOL)isFirstRun
{
    return ![[NSUserDefaults standardUserDefaults] boolForKey:@"isNotFirstRun"];
}

+(void)updateFirstRun:(BOOL)isFirstRun
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:!isFirstRun forKey:@"isNotFirstRun"];
    [userDefaults synchronize];
}
@end
