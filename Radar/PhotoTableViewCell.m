//
//  PhotoTableViewCell.m
//  PhotoDB
//

#import "PhotoTableViewCell.h"

@implementation PhotoTableViewCell
@synthesize creationDateLabel;
@synthesize shutterSpeedLabel;
@synthesize isoSpeedLabel;
@synthesize thumbnail;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
