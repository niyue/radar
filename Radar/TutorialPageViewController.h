//
//  TutorialPageViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>

@interface TutorialPageViewController : UIViewController
{
    UIWebView *webView;
    NSUInteger pageNumber;
    UILabel *pageLabel;
}
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, assign) NSUInteger pageNumber;
@property (nonatomic, retain) IBOutlet UILabel *pageLabel;
@end
