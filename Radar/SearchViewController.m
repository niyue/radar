//
//  SearchViewController.m
//  PhotoDB
//

#import "SearchViewController.h"
#import "SelectDateTimeViewController.h"
#import "PhotoDB.h"
#import "PhotosViewController.h"
#import "UITableView+CellDetailText.h"
#import "ValuePickerViewController.h"

@implementation SearchViewController
@synthesize startDateCell;
@synthesize endDateCell;
@synthesize searchConditionsTable;
@synthesize searchCategory;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[searchCategory identifiers] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger currentRow = [indexPath row];
    NSString *cellIdentifier = [[searchCategory identifiers] objectAtIndex:section];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    int currentIndex = section * 2 + currentRow;
    cell.textLabel.text = [[searchCategory labels] objectAtIndex:currentIndex];
    cell.detailTextLabel.text = [[searchCategory defaultValues] objectAtIndex:currentIndex];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[searchCategory headers] objectAtIndex:section];
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SelectDateSegue"])
    {
        NSIndexPath *indexPath = [searchConditionsTable indexPathForSelectedRow];
        UITableViewCell *cell = [searchConditionsTable cellForRowAtIndexPath:indexPath];
        SelectDateTimeViewController *selectDateTimeViewController = [segue destinationViewController];
        selectDateTimeViewController.targetDateLabel = cell.detailTextLabel;
    }
    else if ([[segue identifier] isEqualToString:@"PickISOSpeedSegue"] || [[segue identifier] isEqualToString:@"PickShutterSpeedSegue"])
    {
        NSIndexPath *indexPath = [searchConditionsTable indexPathForSelectedRow];
        UITableViewCell *cell = [searchConditionsTable cellForRowAtIndexPath:indexPath];

        ValuePickerViewController *valuePickerViewController = [segue destinationViewController];
        valuePickerViewController.values = [[searchCategory values] objectAtIndex:indexPath.section];
        valuePickerViewController.defaultValue = cell.detailTextLabel.text;
        valuePickerViewController.valueLabel = cell.textLabel.text;
        valuePickerViewController.valuePickerViewDelegate = self;
    }
    else if ([[segue identifier] isEqualToString:@"ShowSearchResultSegue"])
    {
        NSMutableArray *params = [[NSMutableArray alloc] init];
        int sections = [searchConditionsTable numberOfSections];
        for (int i=0; i<sections; i++) {
            int rows = [searchConditionsTable numberOfRowsInSection:i];
            for (int j=0; j<rows; j++) {
                [params addObject:[searchConditionsTable detailTextForCell:i cellRow:j]];
            }
        }
        NSArray *matchedPhotos = [searchCategory search:params];
        PhotosViewController *photosViewController = [segue destinationViewController];
        photosViewController.photos = matchedPhotos;
    }
}

#pragma mark - ValuePickerViewDelegate
-(void)didPickValue:(NSString *)pickedValue
{
    NSIndexPath *indexPath = [searchConditionsTable indexPathForSelectedRow];
    UITableViewCell *cell = [searchConditionsTable cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = pickedValue;
}

@end
