//
//  PhotoLocationAnnotation.m
//  PhotoDB
//
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "PhotoLocationAnnotation.h"
#import <math.h>

@implementation PhotoLocationAnnotation
@synthesize coordinate;
@synthesize locationTitle;

- (id)initWithLocation:(CLLocation *)photoLocation
{ 
    self = [super init];
    if(nil != self) {
        self->location = photoLocation;
        self.coordinate = self->location.coordinate;
    }
    return self; 
}

- (NSString *)title
{
    return locationTitle;
}

- (NSString *)subtitle
{
    double latitude = location.coordinate.latitude;
    double longitude = location.coordinate.longitude;
    NSString *latitudeDirection = latitude >= 0 ? @"N" : @"S";
    NSString *longitudeDirection = longitude >= 0 ? @"E" : @"W";
    return [NSString stringWithFormat:@"%@:%1.3f %@:%1.3f", latitudeDirection, fabs(latitude), longitudeDirection, fabs(longitude)];
}
@end
