//
//  SettingsViewController.m
//  PhotoDB
//

#import "SettingsViewController.h"
#import "AppStatus.h"
#import "TutorialViewController.h"

@interface SettingsViewController ()
- (NSString *)indexStatus;
@end

@implementation SettingsViewController
@synthesize settingsTableView;


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // update last index time every time
    [settingsTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    NSString *cellIdentifier = @"IndexPhotoCell";
    if (section == 0) {
        cellIdentifier = @"AboutCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    NSString *defaultFooter = @"You need to index your photos before you search. Only indexed photos can be retrieved in this app.";
    if (section == 1) {
        return [NSString stringWithFormat:@"%@\n\n%@", defaultFooter, [self indexStatus]];
    }
    return nil;
}

- (NSString *)indexStatus
{
    NSString *indexStatus = @"Never indexed before.";
    NSString *lastIndexTime = [AppStatus lastIndexTime];
    if (lastIndexTime != nil) {
        NSInteger lastIndexPhotoNumbers = [AppStatus lastIndexPhotoNumbers];
        indexStatus = [NSString stringWithFormat:@"Last index: %@\n %d photos", lastIndexTime, lastIndexPhotoNumbers];
    }
    return indexStatus;
}

@end
