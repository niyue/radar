//
//  AssetProcessor.h
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@protocol AssetProcessor <NSObject>
- (void)processPhoto:(ALAsset *)photo;
- (void)processGroup:(ALAssetsGroup *)group;
- (void)complete;
@end
