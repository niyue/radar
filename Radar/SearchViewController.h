//
//  SearchViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import "ValuePickerViewDelegate.h"
#import "SearchCategory.h"

@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ValuePickerViewDelegate>

{
    UITableViewCell *startDateCell;
    UITableViewCell *endDateCell;
    UITableView *searchConditionsTable;

    id<SearchCategory> searchCategory;
}
@property (nonatomic, retain) IBOutlet UITableViewCell *startDateCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *endDateCell;
@property (nonatomic, retain) IBOutlet UITableView *searchConditionsTable; 
@property (nonatomic, retain) id<SearchCategory> searchCategory;
@end
