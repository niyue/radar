//
//  PhotoViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "TapDetectingImageView.h"

@interface PhotoViewController : UIViewController <TapDetectingImageViewDelegate, UIScrollViewDelegate>
{
    TapDetectingImageView *photoView;
    ALAsset *photo;
}

@property (nonatomic, retain) IBOutlet TapDetectingImageView *photoView;
@property (nonatomic, retain) ALAsset *photo;
-(IBAction)toggleNavigationBar;

@end
