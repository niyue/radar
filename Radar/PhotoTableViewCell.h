//
//  PhotoTableViewCell.h
//  PhotoDB
//

#import <UIKit/UIKit.h>

@interface PhotoTableViewCell : UITableViewCell
{
    UILabel *creationDateLabel;
    UILabel *shutterSpeedLabel;
    UILabel *isoSpeedLabel;
    UIImageView *thumbnail;
}
@property (nonatomic, strong) IBOutlet UILabel *creationDateLabel;
@property (nonatomic, strong) IBOutlet UILabel *shutterSpeedLabel;
@property (nonatomic, strong) IBOutlet UILabel *isoSpeedLabel;
@property (nonatomic, strong) IBOutlet UIImageView *thumbnail;
@end
