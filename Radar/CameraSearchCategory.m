//
//  CameraSearchCategory.m
//  PhotoDB
//

#import "CameraSearchCategory.h"
#import "PhotoDB.h"

@implementation CameraSearchCategory
@synthesize identifiers;
@synthesize headers;
@synthesize labels;
@synthesize defaultValues;
@synthesize values;
- (CameraSearchCategory *)init
{
    self = [super init];
    if (self != nil) {
        identifiers = [[NSArray alloc] initWithObjects:@"ShutterSpeedSearchCell", @"ISOSpeedSearchCell", nil];
        headers = [[NSArray alloc] initWithObjects:
                   @"Shutter Speed",
                   @"ISO Speed", nil];
        labels = [[NSArray alloc] initWithObjects:
                  @"Min Shutter Speed", @"Max Shutter Speed", 
                  @"Min ISO Speed", @"Max ISO Speed", nil];
        defaultValues = [[NSArray alloc] initWithObjects:
                         @"1/16000", @"1/2", 
                         @"50", @"6400", nil];
        NSArray *shutterSpeeds = [[NSMutableArray alloc] initWithObjects:
                                  @"1/16000",
                                  @"1/4000",
                                  @"1/2000",
                                  @"1/1000",
                                  @"1/500",
                                  @"1/250",
                                  @"1/125",
                                  @"1/60",
                                  @"1/30",
                                  @"1/15",
                                  @"1/8",
                                  @"1/4",
                                  @"1/2",
                                  nil];
        NSArray *isoSpeeds = [[NSMutableArray alloc] initWithObjects:
                              @"50",
                              @"100",
                              @"200",
                              @"400",
                              @"800",
                              @"1600",
                              @"3200",
                              @"6400",
                              nil];
        values = [[NSMutableArray alloc] initWithObjects: shutterSpeeds, isoSpeeds, nil];
    }
    return self;
}

- (NSArray *)search:(NSArray *)params {
    NSInteger maxShutterSpeed = [[[[params objectAtIndex:0] componentsSeparatedByString:@"/"] objectAtIndex:1] intValue];
    NSInteger minShutterSpeed = [[[[params objectAtIndex:1] componentsSeparatedByString:@"/"] objectAtIndex:1] intValue];
    
    NSInteger minISOSpeed = [[params objectAtIndex:2] intValue];
    NSInteger maxISOSpeed = [[params objectAtIndex:3] intValue];
    
    PhotoDB *db = [[PhotoDB alloc] init];
    [db open];
    NSArray *matchedPhotos = [db queryByCamera:minShutterSpeed maxShutterSpeed:maxShutterSpeed minISOSpeed:minISOSpeed maxISOSpeed:maxISOSpeed];
    [db close];
    return matchedPhotos;
}
@end
