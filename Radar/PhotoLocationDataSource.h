//
//  PhotoLocationDataSource.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MapKit/MapKit.h>
#import "PhotoInfoDataSource.h"

@interface PhotoLocationDataSource : NSObject <PhotoInfoDataSource>
{
    ALAsset *photo;
    CLLocation *location;
    NSString *placeName;
    BOOL requireRefresh;
}
@property (nonatomic, retain) CLLocation *location;
- (PhotoLocationDataSource *)initWithLocation:(CLLocation *)photoLocation;
- (NSString *)cellIdentifier;
- (NSString *)nameAtIndex:(int)row;
- (NSString *)textAtIndex:(int)row;
- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *))completionBlock;
- (int)count;
@end
