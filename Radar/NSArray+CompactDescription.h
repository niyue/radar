//
//  NSArray+CompactDescription.h
//

#import <Foundation/Foundation.h>

@interface NSArray (CompactDescription)
- (NSString *)descriptionWithLocale:(id)locale;
@end
