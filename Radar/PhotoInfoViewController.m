//
//  PhotoInfoViewController.m
//  PhotoDB
//

#import "PhotoInfoViewController.h"
#import <ImageIO/CGImageProperties.h>
#import <MapKit/MapKit.h>
#import "NSArray+CompactDescription.h"
#import "PhotoInfoNavigationController.h"
#import "NameValuePairViewController.h"
#import "PhotoMapperViewController.h"
#import "PhotoInfoDataSource.h"
#import "PhotoMetadata.h"

@interface PhotoInfoViewController ()
- (CLLocation *)getLocationInMetadata;
@end

@implementation PhotoInfoViewController
@synthesize metadataTableView;
@synthesize photo;
@synthesize location;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PhotoInfoNavigationController *navController = (PhotoInfoNavigationController *)self.navigationController;
    photo = navController.photo;
    ALAssetRepresentation *representation = [photo defaultRepresentation];
    metadata = [representation metadata];
    self.location = [self getLocationInMetadata];
    
    dataSources = [[NSArray alloc] initWithObjects:
                   [[PhotoPropertiesDataSource alloc] initWithMetadata:metadata],
                   [[PhotoLocationDataSource alloc] initWithLocation:self.location],
                   [[MetadataGroupsDataSource alloc] init],
                   nil];

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)dismiss:(id)sender
{
    [self.presentingViewController dismissModalViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataSources count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dataSources objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    id<PhotoInfoDataSource> dataSource = [dataSources objectAtIndex:section];
    NSString *cellIdentifier = [dataSource cellIdentifier];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    int row = indexPath.row;
    cell.textLabel.text = [dataSource textAtIndex:row];
    [dataSource detailTextAtIndex:row completionHandler:^(NSString *text) {
        cell.detailTextLabel.text = text;
        if ([dataSource requireRefresh]) {
            [self.metadataTableView beginUpdates];
            [self.metadataTableView 
                reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                withRowAnimation:UITableViewRowAnimationNone];
            [self.metadataTableView endUpdates];
        }
    }];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ListMetadataSegue"]) {
        NSIndexPath *selectedRow = [metadataTableView indexPathForSelectedRow];
        MetadataGroupsDataSource *metadataGroupsDataSource = [dataSources objectAtIndex:2];
        NSString *key = [metadataGroupsDataSource groupKey:selectedRow.row];
        NSDictionary *value = [metadata objectForKey:key];
        NameValuePairViewController *pairController = [segue destinationViewController];
        pairController.title = [metadataGroupsDataSource nameAtIndex:selectedRow.row];
        pairController.dict = value;
    }
    else if ([[segue identifier] isEqualToString:@"ShowPhotoLocationSegue"]) {
        PhotoMapperViewController *mapperController = [segue destinationViewController];
        mapperController.location = location;
        mapperController.callOutImage = [[UIImage alloc] initWithCGImage:[photo thumbnail]];
        PhotoMetadata *photoMetadata = [[PhotoMetadata alloc] initWithDict:metadata];
        mapperController.captureDate = [photoMetadata dateTimeOriginal];
    }
}

- (CLLocation *)getLocationInMetadata
{
    NSDictionary *gps = [metadata valueForKey:(NSString*)kCGImagePropertyGPSDictionary];
    NSNumber *laltitude = [gps valueForKey:(NSString *)kCGImagePropertyGPSLatitude];
    NSNumber *longitude = [gps valueForKey:(NSString *)kCGImagePropertyGPSLongitude];
    NSString *laltitudeRef = [gps valueForKey:(NSString *)kCGImagePropertyGPSLatitudeRef];
    NSString *longitudeRef = [gps valueForKey:(NSString *)kCGImagePropertyGPSLongitudeRef];
    
    CLLocationDegrees laltitudeValue = [laltitude doubleValue];
    CLLocationDegrees longitudeValue = [longitude doubleValue];
    if ([laltitudeRef isEqualToString:@"S"]) {
        laltitudeValue = laltitudeValue * -1;
    }
    if ([longitudeRef isEqualToString:@"W"]) {
        longitudeValue = longitudeValue * -1;
    }
    CLLocation *photoLocation = [[CLLocation alloc] initWithLatitude:laltitudeValue longitude:longitudeValue];
    return photoLocation;
}
@end
