//
//  TutorialViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UIScrollViewDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    NSMutableArray *tutorialViewControllers;
    UIBarButtonItem *beginIndexBarButtonItem;
    BOOL indexCompleted;
}
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) NSMutableArray *tutorialViewControllers;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *beginIndexBarButtonItem;
@end
