//
//  PhotoPicker.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface PhotoPicker : NSObject
- (ALAsset *)random;
@end
