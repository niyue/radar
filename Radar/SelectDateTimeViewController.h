//
//  SelectDateTimeViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>

@interface SelectDateTimeViewController : UIViewController <UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    UIDatePicker *datePicker;
    UITableView *dateTimeTableView;
    UILabel *targetDateLabel;
}

@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) IBOutlet UITableView *dateTimeTableView;
@property (nonatomic, retain) UILabel *targetDateLabel;

- (IBAction)dateValueChanged:(id)sender;
@end
