//
//  DateSearchCategory.h
//  PhotoDB
//

#import <Foundation/Foundation.h>
#import "SearchCategory.h"

@interface DateSearchCategory : NSObject <SearchCategory>
{
    NSArray *identifiers;
    NSArray *headers;
    NSArray *labels;
    NSArray *defaultValues;
    NSArray *values;
}
@end
