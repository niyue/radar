//
//  DateHelper.h
//  PhotoDB
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject
+ (NSString *)endOfToday;
+ (NSString *)now;
+ (NSString *)startOfSixMonthsAgo;
+ (NSDate *)valueOf:(NSString *)dateAndMinute;
@end
