//
//  SelectDateTimeViewController.m
//  PhotoDB
//

#import "SelectDateTimeViewController.h"
#import "UITableView+CellDetailText.h"
#import "DateHelper.h"

@implementation SelectDateTimeViewController
@synthesize datePicker;
@synthesize dateTimeTableView;
@synthesize targetDateLabel;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    datePicker.date = [DateHelper valueOf:targetDateLabel.text];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)dateValueChanged:(id)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSIndexPath *indexPath = [dateTimeTableView indexPathForSelectedRow];
    NSUInteger selectedRow = indexPath == nil ? 0 : [indexPath row];
    BOOL isSelectingTime = selectedRow == 1;
    NSString *dateFormat = isSelectingTime ? @"HH:mm" : @"yyyy-MM-dd";
    
    [df setDateFormat:dateFormat];
    NSString *dateTimeString = [df stringFromDate:datePicker.date];
    NSIndexPath *targetIndex = [NSIndexPath indexPathForRow:selectedRow inSection:0];
    UITableViewCell *targetCell = [dateTimeTableView cellForRowAtIndexPath:targetIndex];
    targetCell.detailTextLabel.text = dateTimeString;
    
    targetDateLabel.text = [NSString stringWithFormat:@"%@ %@", 
                            [dateTimeTableView detailTextForCell:0], 
                            [dateTimeTableView detailTextForCell:1]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"DateTimeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSUInteger selectedRow = [indexPath row];
    NSArray *dateAndTime = [targetDateLabel.text componentsSeparatedByCharactersInSet:
                    [NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (selectedRow == 0) {
        cell.textLabel.text = @"Date";
        cell.detailTextLabel.text = [dateAndTime objectAtIndex:0];
    }
    else {
        cell.textLabel.text = @"Time";
        cell.detailTextLabel.text = [dateAndTime objectAtIndex:1];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger selectedRow = [indexPath row];
    datePicker.datePickerMode = selectedRow == 0 ? UIDatePickerModeDate : UIDatePickerModeTime;
    NSDate *targetDate = [DateHelper valueOf:targetDateLabel.text];
    datePicker.date = targetDate;
}

@end
