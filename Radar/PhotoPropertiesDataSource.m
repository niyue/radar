//
//  PhotoPropertiesDataSource.m
//  PhotoDB
//
#import <ImageIO/CGImageProperties.h>
#import "PhotoPropertiesDataSource.h"
#import "NSArray+CompactDescription.h"
#import "PhotoMetadata.h"
#include <math.h>

@implementation PhotoPropertiesDataSource

- (PhotoPropertiesDataSource *)initWithMetadata:(NSDictionary *)metadata
{
    self = [super init];
    if (self) {
        PhotoMetadata *photoMetadata = [[PhotoMetadata alloc] initWithDict:metadata];
        
        NSString *exifDateTimeOriginal = [photoMetadata dateTimeOriginal];
        NSString *shutterSpeed = [NSString stringWithFormat:@"1/%d s", [photoMetadata shutterSpeed]];
        
        NSString *focalLength = [NSString stringWithFormat:@"%1.2f mm", [photoMetadata focalLength]];
        NSString *aperture = [NSString stringWithFormat:@"f/%1.1f", [photoMetadata fnumber]];
        NSString *isoSpeed = [NSString stringWithFormat:@"%d", [photoMetadata isoSpeed]];
        
        properties = [[NSArray alloc] initWithObjects:
                      @"Date",
                      @"Focal Length",
                      @"Aperture",
                      @"Shutter Speed",
                      @"ISO Speed",
                      nil];
        
        values = [[NSArray alloc] initWithObjects: 
                    exifDateTimeOriginal,
                    focalLength,
                    aperture,
                    shutterSpeed,
                    isoSpeed,
                    nil];
    }
    return self;
}

- (NSString *)cellIdentifier
{
    return @"PhotoInfoCell";
}

- (NSString *)nameAtIndex:(int)row
{
    return [properties objectAtIndex:row];
}

- (NSString *)textAtIndex:(int)row
{
    return [properties objectAtIndex:row];
}

- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *))completionBlock
{
    NSString *value = [values objectAtIndex:row];
    completionBlock(value);
    return value;
}

- (int)count
{
    return [properties count];
}

- (BOOL)requireRefresh
{
    return NO;
}
@end
