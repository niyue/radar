//
//  NameValuePairViewController.h
//

#import <UIKit/UIKit.h>

@interface NameValuePairViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSDictionary *dict;
}
@property (nonatomic, retain) NSDictionary *dict;
@end
