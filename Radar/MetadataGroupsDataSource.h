//
//  MetadataGroupsDataSource.h
//

#import <Foundation/Foundation.h>
#import "PhotoInfoDataSource.h"

@interface MetadataGroupsDataSource : NSObject <PhotoInfoDataSource>
{
    NSArray *metadataGroupNames;
    NSArray *groupKeys;
}
- (MetadataGroupsDataSource *)init;
- (NSString *)cellIdentifier;
- (NSString *)nameAtIndex:(int)row;
- (NSString *)textAtIndex:(int)row;
- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *))completionBlock;
- (NSString *)groupKey:(int)row;
- (int)count;
@end
