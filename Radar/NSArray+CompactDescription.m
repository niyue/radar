//
//  NSArray+CompactDescription.m
//

#import "NSArray+CompactDescription.h"

@implementation NSArray (CompactDescription)
- (NSString *)descriptionWithLocale:(id)locale {
    NSString *values = [self componentsJoinedByString:@","];
    return [NSString stringWithFormat:@"[%@]", values];
}
@end
