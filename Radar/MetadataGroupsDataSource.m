//
//  MetadataGroupsDataSource.m
//

#import "MetadataGroupsDataSource.h"
#import <ImageIO/CGImageProperties.h>

@implementation MetadataGroupsDataSource
- (MetadataGroupsDataSource *)init
{
    self = [super init];
    if (self) {
        metadataGroupNames = [[NSArray alloc] initWithObjects:
                              @"EXIF", 
                              @"TIFF", 
                              @"GPS", 
                              nil];
        groupKeys = [[NSArray alloc] initWithObjects:
                        (NSString *)kCGImagePropertyExifDictionary,
                        (NSString *)kCGImagePropertyTIFFDictionary,
                        (NSString *)kCGImagePropertyGPSDictionary,
                        nil];
    }
    return self;
}

- (NSString *)cellIdentifier
{
    return @"MetadataGroupCell";
}

- (NSString *)nameAtIndex:(int)row
{
    return [metadataGroupNames objectAtIndex:row];
}

- (NSString *)textAtIndex:(int)row
{
    return [metadataGroupNames objectAtIndex:row];
}

- (NSString *)detailTextAtIndex:(int)row completionHandler:(void (^)(NSString *))completionBlock
{
    NSString *text = @"";
    completionBlock(text);
    return text;
}

- (int)count
{
    return [metadataGroupNames count];
}

- (NSString *)groupKey:(int)row
{
    return [groupKeys objectAtIndex:row];
}

- (BOOL)requireRefresh
{
    return NO;
}
@end
