//
//  RandomPhotoController.h
//  PhotoDB
//
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface RandomPhotoController : UIViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate>
{
    UIImageView *imageView;
    ALAsset *photo;
    UIActivityIndicatorView *loadingIndicator;
    UIView *hudView;
    UISwipeGestureRecognizer *swipeLeftGestureRecognizer;
    UISwipeGestureRecognizer *swipeRightGestureRecognizer;
}
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, retain) IBOutlet UIView *hudView;
- (IBAction)nextRandomPhoto:(id)sender;
@property (nonatomic, retain) IBOutlet UISwipeGestureRecognizer *swipeLeftGestureRecognizer;
@property (nonatomic, retain) IBOutlet UISwipeGestureRecognizer *swipeRightGestureRecognizer;
@end
