//
//  CompoundSearchCategory.m
//  PhotoDB
//

#import "CompoundSearchCategory.h"
#import "SearchCategory.h"
#import "DateSearchCategory.h"
#import "CameraSearchCategory.h"
#import "PhotoDB.h"


@implementation CompoundSearchCategory
@synthesize identifiers;
@synthesize headers;
@synthesize labels;
@synthesize defaultValues;
@synthesize values;
- (CompoundSearchCategory *)init
{
    self = [super init];
    if (self != nil) {
        id<SearchCategory> dateSearchCategory = [[DateSearchCategory alloc] init];
        id<SearchCategory> cameraSearchCategory = [[CameraSearchCategory alloc] init];
        NSArray *categories = [[NSArray alloc] initWithObjects:dateSearchCategory, cameraSearchCategory, nil];
        
        identifiers = [[NSMutableArray alloc] init];
        headers = [[NSMutableArray alloc] init];
        labels = [[NSMutableArray alloc] init];
        defaultValues = [[NSMutableArray alloc] init];
        values = [[NSMutableArray alloc] init];
        
        for (id<SearchCategory> searchCategory in categories) {
            [identifiers addObjectsFromArray:[searchCategory identifiers]];
            [headers addObjectsFromArray:[searchCategory headers]];
            [labels addObjectsFromArray:[searchCategory labels]];
            [defaultValues addObjectsFromArray:[searchCategory defaultValues]];
            [values addObjectsFromArray:[searchCategory values]];
        }
    }
    return self;
}

- (NSArray *)search:(NSArray *)params {
    NSString *startDateTime = [NSString stringWithFormat:@"%@:00", [params objectAtIndex:0]];
    NSString *endDateTime = [NSString stringWithFormat:@"%@:59", [params objectAtIndex:1]];
    
    NSInteger maxShutterSpeed = [[[[params objectAtIndex:2] componentsSeparatedByString:@"/"] objectAtIndex:1] intValue];
    NSInteger minShutterSpeed = [[[[params objectAtIndex:3] componentsSeparatedByString:@"/"] objectAtIndex:1] intValue];
    
    NSInteger minISOSpeed = [[params objectAtIndex:4] intValue];
    NSInteger maxISOSpeed = [[params objectAtIndex:5] intValue];
    
    PhotoDB *db = [[PhotoDB alloc] init];
    [db open];
    NSArray *matchedPhotos = [db query:startDateTime toDate:endDateTime minShutterSpeed:minShutterSpeed maxShutterSpeed:maxShutterSpeed minISOSpeed:minISOSpeed maxISOSpeed:maxISOSpeed];
    [db close];
    return matchedPhotos;
}
@end
