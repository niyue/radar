//
//  SearchCategoryViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>

@interface SearchCategoryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView *searchCategoriesTable;
    NSArray *searchCategoryIdentifiers;
}
@property (nonatomic, retain) IBOutlet UITableView *searchCategoriesTable;
@end
