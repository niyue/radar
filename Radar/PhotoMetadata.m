//
//  PhotoMetadata.m
//  PhotoDB
//

#import "PhotoMetadata.h"
#import <ImageIO/CGImageProperties.h>
#include <math.h>

@interface PhotoMetadata ()
- (NSString *)formatDate:(NSDate *)date;
- (NSString *)formatTime:(NSDate *)date;
- (NSDate *)parseDate:(NSString *)dateString;
@end

@implementation PhotoMetadata
{
    NSDateFormatter *dateFormatter;
    NSDateFormatter *exifDateFormatter;
}
-(PhotoMetadata *)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self != nil) {
        if (dict == nil) {
            dict = [[NSDictionary alloc] init];
        }
        metadata = dict;
        exif = [metadata valueForKey:(NSString*)kCGImagePropertyExifDictionary];
    }
    return self;
}

-(NSString *)dateTimeOriginal
{
    NSString *creationDateString = nil;
    if (exif != nil) {
        NSString *exifDateTimeOriginal = [exif valueForKey:(NSString*)kCGImagePropertyExifDateTimeOriginal];
        NSDate *dateTimeOriginal = [self parseDate:exifDateTimeOriginal];
        creationDateString = [self formatDate:dateTimeOriginal];
    }
    return creationDateString;
}

-(NSString *)timeOriginal
{
    NSString *creationTimeString = nil;
    if (exif != nil) {
        NSString *exifDateTimeOriginal = [exif valueForKey:(NSString*)kCGImagePropertyExifDateTimeOriginal];
        NSDate *dateTimeOriginal = [self parseDate:exifDateTimeOriginal];
        creationTimeString = [self formatTime:dateTimeOriginal];
    }
    return creationTimeString;
}

-(NSInteger)shutterSpeed
{
    NSNumber *shutterSpeedInExif = [exif valueForKey:(NSString *)kCGImagePropertyExifShutterSpeedValue];
    NSInteger shutterSpeedValue = pow(2, [shutterSpeedInExif doubleValue]);
    return shutterSpeedValue;
}

-(NSInteger)isoSpeed
{
    NSArray *isoSpeedRatings = [exif valueForKey:(NSString*)kCGImagePropertyExifISOSpeedRatings];
    return [((NSNumber *) [isoSpeedRatings objectAtIndex:0]) intValue];
}

-(double)focalLength
{
    return [((NSNumber *)[exif valueForKey:(NSString*)kCGImagePropertyExifFocalLength]) doubleValue];
}

-(double)fnumber
{
    return [((NSNumber *)[exif valueForKey:(NSString*)kCGImagePropertyExifFNumber]) doubleValue];
}

#pragma mark - private methods

- (NSString *)formatDate:(NSDate *)date {
    if(dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    }
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];    
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (NSString *)formatTime:(NSDate *)date {
    if(dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    }
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (NSDate *)parseDate:(NSString *)dateString {
    if(exifDateFormatter == nil) {
        exifDateFormatter = [[NSDateFormatter alloc] init];
        [exifDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [exifDateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];  
    }
    NSDate *date = [exifDateFormatter dateFromString:dateString];
    return date;
}
@end
