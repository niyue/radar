//
//  PhotoIndexingViewController.m
//  PhotoDB
//
//

#import "PhotoIndexingViewController.h"
#import "LayerAnimation.h"
#import "AppStatus.h"
#import "DateHelper.h"

@interface PhotoIndexingViewController ()
-(void)updateProgressView;
-(void)updateImageView:(ALAsset *)photo;
@end

@implementation PhotoIndexingViewController
@synthesize processingProgressView;
@synthesize currentPhotoView;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self->photoDB = [[PhotoDB alloc] init];
    [self->photoDB clean];
    [self->photoDB open];
    
    self->assetReader = [[AssetsReader alloc] init];
    self->assetReader.assetProcessor = self;
    
    NSBlockOperation* loadAssetsOp = [NSBlockOperation blockOperationWithBlock: ^{
        [self->assetReader photos];
    }];
    
    NSOperationQueue *opQueue = [[NSOperationQueue alloc] init];
    [opQueue addOperation:loadAssetsOp];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)updateProgressView {
    self.processingProgressView.progress = ((float) self->processedPhotos) / self->numberOfPhotos;
}

- (void)updateImageView:(ALAsset *)photo {
    if (self->processedPhotos == 1 || self->processedPhotos % 5 == 0) {
        CGImageRef fullScreenImage = [[photo defaultRepresentation] fullScreenImage];
        
        [LayerAnimation fadeIn:self.currentPhotoView];
        self.currentPhotoView.image = [[UIImage alloc] initWithCGImage:fullScreenImage];
    }
}

#pragma mark - alert
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AssetProcessor
- (void)processPhoto:(ALAsset *)photo {
    [self->photoDB indexPhoto:photo];
    self->processedPhotos++;
    [self performSelectorOnMainThread:@selector(updateImageView:) withObject:photo waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(updateProgressView) withObject:nil waitUntilDone:NO];
}

- (void)processGroup:(ALAssetsGroup *)group {
    self->numberOfPhotos = [group numberOfAssets];
}

- (void)complete {
    [self->photoDB close];
    [AppStatus updateIndex:[DateHelper now] photoNumber:self->numberOfPhotos];
    UIAlertView *alert =  [[UIAlertView alloc] 
     initWithTitle:@"Indexing completed" 
     message:[NSString stringWithFormat:@"%ld photos are indexed.", (long)self->numberOfPhotos]
     delegate:self 
     cancelButtonTitle:@"OK" 
     otherButtonTitles:nil];
    [alert show];
}

@end
