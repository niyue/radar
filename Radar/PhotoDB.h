//
//  PhotoDB.h
//  PhotoDB
//
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@interface PhotoDB : NSObject
- (BOOL)open;
- (BOOL)indexPhotos:(NSArray *)photos;
- (BOOL)indexPhoto:(ALAsset *)photo;
- (int)count;
- (NSArray *)queryByDate:(NSString *)from toDate:(NSString *)to;
- (NSArray *)queryByDateTime:(NSString *)from fromTime:(NSString *)fromTime toDate:(NSString *)to toTime:(NSString *)toTime;
- (NSArray *)query:(NSString *)fromDate toDate:(NSString *)toDate minShutterSpeed:(NSInteger)minShutterSpeed maxShutterSpeed:(NSInteger)maxShutterSpeed minISOSpeed:(NSInteger)minISOSpeed maxISOSpeed:(NSInteger)maxISOSpeed;
- (NSArray *)queryByCamera:(NSInteger)minShutterSpeed maxShutterSpeed:(NSInteger)maxShutterSpeed minISOSpeed:(NSInteger)minISOSpeed maxISOSpeed:(NSInteger)maxISOSpeed;
- (NSDictionary *)photoAtIndex:(int)index;
- (BOOL)close;
- (BOOL)clean;
- (NSString *)dbLocation;
@end
