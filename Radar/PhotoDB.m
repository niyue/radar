//
//  PhotoDB.m
//  PhotoDB
//

#import "PhotoDB.h"
#import <ImageIO/CGImageProperties.h>
#import "ALAsset+AssetURL.h"
#import "PhotoMetadata.h"

@interface PhotoDB ()
- (NSArray *)convertResultSet:(FMResultSet *)resultSet;
@end

@implementation PhotoDB
{
    FMDatabase *db;
    NSDateFormatter *dateFormatter;
    NSDateFormatter *exifDateFormatter;
}

- (BOOL)open {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dbPath = [self dbLocation];
    BOOL dbExisted = [fileManager fileExistsAtPath:dbPath];
    db = [FMDatabase databaseWithPath:dbPath];
    BOOL dbOpened = [db open];
    if(!dbExisted) {
        BOOL tableCreated = [db executeUpdate:@"CREATE TABLE photos (url text, creation_date text, creation_time text, shutter_speed interger, iso_speed integer)"];
        NSLog(@"Create photos database table %@", tableCreated ? @"YES" : @"NO");
    }
    return dbOpened;
}

- (BOOL)indexPhotos:(NSArray *)photos {
    NSEnumerator *photoEnumerator = [photos objectEnumerator];
    BOOL success = true;
    ALAsset *photo;
    while ((photo = [photoEnumerator nextObject])) {
        success = success && [self indexPhoto:photo];
    }
    return success;
}

- (BOOL)indexPhoto:(ALAsset *)photo {
    @autoreleasepool {
        ALAssetRepresentation *representation = [photo defaultRepresentation];
        BOOL success = YES;
        if (representation != nil) {
            NSDictionary *metadata = [representation metadata];
            
            if (metadata != nil) {
                PhotoMetadata *photoMetadata = [[PhotoMetadata alloc] initWithDict:metadata];
                NSString *creationDate = [photoMetadata dateTimeOriginal];
                NSString *creationTime = [photoMetadata timeOriginal];
                int shutterSpeed = [photoMetadata shutterSpeed];
                int isoSpeed = [photoMetadata isoSpeed]; 
                if (creationDate != nil) {
                    NSURL *url = [photo url];
                    NSLog(@"Adding photo %@ into database", url);
                    
                    success = [db executeUpdateWithFormat:@"INSERT INTO photos VALUES (%@, %@, %@, %d, %d)", 
                               url, 
                               creationDate,
                               creationTime,
                               shutterSpeed,
                               isoSpeed];
                    if (!success) {
                        NSLog(@"Fail to inserting photo %@", url);
                    }
                }
            }
        }    
        return success;
    }
}


- (NSArray *)queryByDate:(NSString *)from toDate:(NSString *)to {
    FMResultSet *result = 
        [db executeQuery:@"SELECT * FROM photos WHERE creation_date >= ? AND creation_date <= ?", from, to];
    NSArray * photos = [self convertResultSet:result];
    return photos;
}

- (NSArray *)queryByDateTime:(NSString *)from fromTime:(NSString *)fromTime toDate:(NSString *)to toTime:(NSString *)toTime {
    FMResultSet *result = 
        [db executeQuery:@"SELECT * FROM photos WHERE creation_date >= ? AND creation_time >= ? AND creation_date <= ? AND creation_time <= ?", from, fromTime, to, toTime];
    NSArray * photos = [self convertResultSet:result];
    return photos;
}

- (NSArray *)queryByCamera:(NSInteger)minShutterSpeed maxShutterSpeed:(NSInteger)maxShutterSpeed minISOSpeed:(NSInteger)minISOSpeed maxISOSpeed:(NSInteger)maxISOSpeed {
    FMResultSet *result = 
    [db executeQueryWithFormat:@"SELECT * FROM photos WHERE shutter_speed >= %d AND shutter_speed <= %d AND iso_speed >= %d AND iso_speed <= %d", minShutterSpeed, maxShutterSpeed, minISOSpeed, maxISOSpeed];
    NSArray * photos = [self convertResultSet:result];
    return photos;
}

- (NSArray *)query:(NSString *)fromDate toDate:(NSString *)toDate minShutterSpeed:(NSInteger)minShutterSpeed maxShutterSpeed:(NSInteger)maxShutterSpeed minISOSpeed:(NSInteger)minISOSpeed maxISOSpeed:(NSInteger)maxISOSpeed {
    FMResultSet *result = 
    [db executeQueryWithFormat:@"SELECT * FROM photos WHERE creation_date >= %@ AND creation_date <= %@ AND shutter_speed >= %d AND shutter_speed <= %d AND iso_speed >= %d AND iso_speed <= %d", fromDate, toDate, minShutterSpeed, maxShutterSpeed, minISOSpeed, maxISOSpeed];
    NSArray * photos = [self convertResultSet:result];
    return photos;
}

- (NSDictionary *)photoAtIndex:(int)index {
    FMResultSet *result = [db executeQueryWithFormat:@"SELECT * FROM photos LIMIT 1 OFFSET %d", index];
    NSArray * photos = [self convertResultSet:result];
    return [photos objectAtIndex:0];
}

- (NSArray *)convertResultSet:(FMResultSet *)resultSet {
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    while ([resultSet next]) {
        NSString *url = [resultSet stringForColumn:@"url"];
        NSDate *creationDate = [resultSet dateForColumn:@"creation_date"];
        NSString *creationTime = [resultSet stringForColumn:@"creation_time"];
        NSMutableDictionary *photo = [[NSMutableDictionary alloc] init];
        [photo setValue:url forKey:@"url"];
        [photo setValue:creationDate forKey:@"creationDate"];
        [photo setValue:creationTime forKey:@"creationTime"];
        [photos addObject:photo];
    }
    return photos;
}

- (int)count {
    FMResultSet *result = [db executeQuery:@"SELECT COUNT(*) AS photo_count FROM photos"];
    int count = 0;
    if([result next]) {
        count = [result intForColumn:@"photo_count"];
    }
    return count;
}

- (BOOL)close {
    return [db close];
}

- (BOOL)clean {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dbPath = [self dbLocation];
    BOOL dbExisted = [fileManager fileExistsAtPath:dbPath];
    if(dbExisted) {
        [fileManager removeItemAtPath:dbPath error:nil];
    }
    return dbExisted;
}

// private methods



- (NSString *)dbLocation {    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(
                            NSDocumentDirectory, 
                            NSUserDomainMask, YES);
    
    NSString *documentDir = [dirPaths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/PhotoDB.db", documentDir];
}
@end
