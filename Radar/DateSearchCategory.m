//
//  DateSearchCategory.m
//  PhotoDB
//

#import "DateSearchCategory.h"
#import "PhotoDB.h"
#import "DateHelper.h"

@implementation DateSearchCategory
@synthesize identifiers;
@synthesize headers;
@synthesize labels;
@synthesize defaultValues;
@synthesize values;
- (DateSearchCategory *)init
{
    self = [super init];
    if (self != nil) {
        identifiers = [[NSArray alloc] initWithObjects:@"DateSearchCell", nil];
        headers = [[NSArray alloc] initWithObjects:@"Capture Date", nil];
        labels = [[NSArray alloc] initWithObjects:@"Start Date", @"End Date", nil];
        NSString *endOfToday = [DateHelper endOfToday];
        NSString *startOfSixMonthsAgo = [DateHelper startOfSixMonthsAgo];
        defaultValues = [[NSArray alloc] initWithObjects:startOfSixMonthsAgo, endOfToday, nil];
        NSArray *dates = [[NSMutableArray alloc] init];
        values = [[NSMutableArray alloc] initWithObjects: dates, nil];
    }
    return self;
}

- (NSArray *)search:(NSArray *)params {
    NSString *startDateTime = [NSString stringWithFormat:@"%@:00", [params objectAtIndex:0]];
    NSString *endDateTime = [NSString stringWithFormat:@"%@:59", [params objectAtIndex:1]];
    
    PhotoDB *db = [[PhotoDB alloc] init];
    [db open];
    NSArray *matchedPhotos = [db queryByDate:startDateTime toDate:endDateTime];
    [db close];
    return matchedPhotos;
}
@end
