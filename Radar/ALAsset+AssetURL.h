//
//  ALAsset+AssetURL.h
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAsset (AssetURL)
- (NSURL *)url;
@end
