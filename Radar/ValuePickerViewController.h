//
//  SelectShutterSpeedViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import "ValuePickerViewDelegate.h"

@interface ValuePickerViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    UIPickerView *pickerView;
    NSMutableArray *values;
    NSString *valueLabel;
    NSString *defaultValue;
    UITableView *valueTableView;
    id<ValuePickerViewDelegate> valuePickerViewDelegate;
}
@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray *values;
@property (nonatomic, retain) NSString *valueLabel;
@property (nonatomic, retain) NSString *defaultValue;
@property (nonatomic, retain) IBOutlet UITableView *valueTableView;
@property (nonatomic, retain) id<ValuePickerViewDelegate> valuePickerViewDelegate;
@end

