//
//  TutorialPageViewController.m
//  PhotoDB
//

#import "TutorialPageViewController.h"

@interface TutorialPageViewController ()

@end

@implementation TutorialPageViewController
@synthesize webView;
@synthesize pageNumber;
@synthesize pageLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *helpPage = [NSString stringWithFormat:@"help-%d", pageNumber];
    NSString *path = [[NSBundle mainBundle] pathForResource:helpPage ofType:@"html"];
    
	NSURL *instructionsURL = [NSURL fileURLWithPath:path];
	[webView loadRequest:[NSURLRequest requestWithURL:instructionsURL]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
