//
//  TutorialViewController.m
//  PhotoDB
//

#import "TutorialViewController.h"
#import "TutorialPageViewController.h"
#import "AppStatus.h"

@implementation TutorialViewController
@synthesize pageControl;
@synthesize tutorialViewControllers;
@synthesize scrollView;
@synthesize beginIndexBarButtonItem;

static NSUInteger kNumberOfPages = 3;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [controllers addObject:[NSNull null]];
    }
    
    self.tutorialViewControllers = controllers;
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
    
    pageControl.numberOfPages = kNumberOfPages;
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    self->indexCompleted = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self->indexCompleted) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)scrollViewDidScroll:(UIScrollView *)theScrollView {
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = theScrollView.frame.size.width;
    int page = floor((theScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    if (page == kNumberOfPages-1) {
        self.beginIndexBarButtonItem.enabled = YES;
    }
}

- (void)loadScrollViewWithPage:(int)page {
    if (page >= 0 && page < kNumberOfPages) {
        // replace the placeholder if necessary
        TutorialPageViewController *controller = [tutorialViewControllers objectAtIndex:page];
        if ((NSNull *)controller == [NSNull null]) {
            UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            controller = [storybord instantiateViewControllerWithIdentifier:@"TheTutorialPageViewController"];
            controller.pageNumber = page;
            [tutorialViewControllers replaceObjectAtIndex:page withObject:controller];
        }
        
        // add the controller's view to the scroll view
        if (controller.view.superview == nil) {
            CGRect frame = scrollView.frame;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            controller.view.frame = frame;
            [scrollView addSubview:controller.view];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"BeginIndexSegue"]) {
        [AppStatus updateFirstRun:NO];
        self->indexCompleted = YES;
    }
}

@end
