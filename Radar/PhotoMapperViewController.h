//
//  PhotoMapperViewController.h
//  PhotoDB
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface PhotoMapperViewController : UIViewController <MKMapViewDelegate>
{
    MKMapView *mapView;
    CLLocation *location;
    UIImage *callOutImage;
    NSString *captureDate;
}
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) CLLocation *location;
@property (nonatomic, retain) UIImage *callOutImage;
@property (nonatomic, retain) NSString *captureDate;
@end
