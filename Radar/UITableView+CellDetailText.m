//
//  UITableView+CellDetailText.m
//  PhotoDB
//

#import "UITableView+CellDetailText.h"

@implementation UITableView (CellDetailText)
- (NSString *)detailTextForCell:(NSUInteger)cellRow
{
    return [self detailTextForCell:0 cellRow:cellRow];
}
- (NSString *)detailTextForCell:(NSUInteger)cellSection cellRow:(NSUInteger)cellRow
{
    NSIndexPath *cellIndex = [NSIndexPath indexPathForRow:cellRow inSection:cellSection];
    UITableViewCell *cell = [self cellForRowAtIndexPath:cellIndex];
    return cell.detailTextLabel.text;
}
@end
