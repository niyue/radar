//
//  PhotoInfoNavigationController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface PhotoInfoNavigationController : UINavigationController
{
    ALAsset *photo;
}
@property (nonatomic, retain) ALAsset *photo;
@end
