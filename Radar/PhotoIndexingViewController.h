//
//  PhotoIndexingViewController.h
//  PhotoDB
//

#import <UIKit/UIKit.h>
#import "AssetProcessor.h"
#import "PhotoDB.h"
#import "AssetsReader.h"

@interface PhotoIndexingViewController : UIViewController <AssetProcessor, UIAlertViewDelegate>
{
    NSInteger numberOfPhotos;
    NSInteger processedPhotos;
    PhotoDB *photoDB;
    AssetsReader *assetReader;
    UIProgressView *processingProgressView;
    UIImageView *currentPhotoView;
}

@property (nonatomic, retain) IBOutlet UIProgressView *processingProgressView;
@property (nonatomic, retain) IBOutlet UIImageView *currentPhotoView;

@end
